<?php
require_once('inc/common.php');

// config syntax definition
$syntax_base = [ 'datadir'            => [ 'mandatory' => FALSE,
                                           'default'   => '/var/lib/collectd',
                                           'check'     => 'is_dir'
                                         ],
                 'rrdtool-options'    => [ 'mandatory' => FALSE,
                                           'default'   => [ '--slope-mode',
                                                            '--color', 'BACK#F0F0F0',
                                                            '--color', 'FRAME#F0F0F0',
                                                            '--color', 'CANVAS#FFFFFF',
                                                            '--color', 'FONT#666666',
                                                            '--color', 'AXIS#CFD6F8',
                                                            '--color', 'ARROW#CFD6F8',
                                                            '--border', '0' ],
                                           'check'     => 'is_string_array'
                                         ],
                 'colors'             => [ 'mandatory' => FALSE,
                                           'default'   => [ '00CC00', '0066B3', 'FF8000', 'FFCC00', '330099', '990099', 'CCFF00', 'FF0000',
                                                            '808080', '008F00', '00487D', 'B35A00', 'B38F00', '6B006B', '8FB300', 'B30000',
                                                            'BEBEBE', '80FF80', '80C9FF', 'FFC080', 'FFE680', 'AA80FF', 'EE00CC', 'FF8080',
                                                            '666600', 'FFBFFF', '00FFCC', 'CC6699', '999900' ],
                                           'check'     => 'is_hexcolor_array'
                                         ],
                 'hosts'              => [ 'mandatory' => FALSE,
                                           'default'   => NULL,
                                           'check'     => 'is_hosts'
                                         ],
                 'timespans-overview' => [ 'mandatory' => FALSE,
                                           'default'   => [ 'day' => '24h', 'week' => '7d' ],
                                           'check'     => 'is_timespan_hash',
                                           'normalize' => 'normalize_timespan_hash'
                                         ],
                 'timespans-detail'   => [ 'mandatory' => FALSE,
                                           'default'   => [ 'hour' => '1h', 'month' => '31d', 'year' => '365d' ],
                                           'check'     => 'is_timespan_hash',
                                           'normalize' => 'normalize_timespan_hash'
                                         ],
                 'refresh'            => [ 'mandatory' => FALSE,
                                           'default'   => 300,
                                           'check'     => 'is_int'
                                         ],
];


$syntax_template = [ 'disabled'       => [ 'mandatory' => FALSE,
                                           'default'   => FALSE,
                                           'check'     => 'is_bool'
                                         ],
                     'position'       => [ 'mandatory' => FALSE,
                                           'default'   => 999999,
                                           'check'     => 'is_int'
                                         ],
                     'title'          => [ 'mandatory' => FALSE,
                                           'default'   => 'missing title',
                                           'check'     => 'is_string'
                                         ],
                     'vertical-label' => [ 'mandatory' => FALSE,
                                           'default'   => 'missing vertical-label',
                                           'check'     => 'is_string'
                                         ],
                     'hrules'         => [ 'mandatory' => FALSE,
                                           'default'   => [],
                                           'check'     => 'is_hrule_array'
                                         ],
                     'lower-limit'    => [ 'mandatory' => FALSE,
                                           'default'   => NULL,
                                           'check'     => 'is_limit'
                                         ],
                     'upper-limit'    => [ 'mandatory' => FALSE,
                                           'default'   => NULL,
                                           'check'     => 'is_limit'
                                         ],
                     'metric-prefix'  => [ 'mandatory' => FALSE,
                                           'default'   => FALSE,
                                           'check'     => 'is_bool'
                                         ],
                     'style'          => [ 'mandatory' => FALSE,
                                           'default'   => 'lines',
                                           'check'     => 'is_style'
                                         ],
                     'mirror-colors'  => [ 'mandatory' => FALSE,
                                           'default'   => FALSE,
                                           'check'     => 'is_bool'
                                         ],
                     'factor'         => [ 'mandatory' => FALSE,
                                           'default' => 1,
                                           'check' => 'is_number'
                                         ],
                     'multigraph'     => [ 'mandatory' => FALSE,
                                           'default'   => NULL,
                                           'check'     => 'is_multigraph'
                                         ],
                     'rules+'         => [ 'mandatory' => FALSE,
                                           'default'   => [],
                                           'check'     => 'is_array',
                                         ],
                     'rules-'         => [ 'mandatory' => FALSE,
                                           'default'   => [],
                                           'check'     => 'is_array',
                                         ],
];

$syntax_rule = [ 'source'          => [ 'mandatory' => TRUE,
                                        'check' => 'is_source'
                                      ],
                 'legend'          => [ 'mandatory' => FALSE,
                                        'default' => '%P-%p/%T-%t/%D',
                                        'check' => 'is_string'
                                      ],
];








// load a JSON configuration file into a PHP hash
function config_read($path)
{
    // read json file
    $raw_content = @file_get_contents($path);
    if ($raw_content === FALSE) { fatal_error(500, 'Failed to read config file "' . $path . '"'); }

    // strip comments
    $lines = explode("\n", $raw_content);
    foreach($lines as $i => &$line) { $line = preg_replace('#^\s*//.*$#', '', $line); }
    $raw_content = implode("\n", $lines);

    // convert the json to a PHP associative array
    $config = json_decode($raw_content, TRUE);
    if (json_last_error() != JSON_ERROR_NONE) { fatal_error(500, 'JSON error in config file "' . $path . '": ' . json_last_error_msg()); }

    return($config);
}




// load the main configuration
function config_main()
{
    // read main config
    $path = 'config/config.json';
    $config = config_read($path);

    // check syntax
    $config = config_chunk_check($config, $GLOBALS['syntax_base']);
    if (is_string($config)) { fatal_error(500, 'Syntax error in config file "' . $path . '": ' . $config); }

    // make sure timespans-detail contains timespans-overview
    $config['timespans-detail'] = array_merge($config['timespans-overview'], $config['timespans-detail']);

    // sort timespans by duration
    asort($config['timespans-overview']);    
    asort($config['timespans-detail']);
    
    return($config);
}




// load the templates: main templates + host override
function config_templates($host)
{
    // read main templates
    $path = 'config/templates.json';
    $templates = config_read($path);

    // check syntax
    $templates = config_templates_check($templates);
    if (is_string($templates)) { fatal_error(500, 'Syntax error in templates file "' . $path . '": ' . $templates); }

    // load host's override, if any
    $path = 'config/templates-' . $host . '.json';
    if (! file_exists($path)) { return($templates); }
    $override = config_read($path);

    // check syntax
    $override = config_templates_check($override, TRUE);
    if (is_string($override)) { fatal_error(500, 'Syntax error in templates file "' . $path . '": ' . $override); }

    // merge main and override
    foreach($override as $template_name => $template)
    {
        // new template is just copied
        if (! isset($templates[$template_name])) { $templates[$template_name] = $template; }

        // existing template is merged
        else { $templates[$template_name] = array_merge($templates[$template_name], $template); }
    }

    // check syntax again, in case the override added new templates, we need to fill missing options with default values
    $templates = config_templates_check($templates);
    if (is_string($templates)) { fatal_error(500, 'Syntax error in templates file "' . $path . '": ' . $templates); }

    // all good
    return($templates);
}




// enforce a given syntax on a hash
function config_chunk_check($chunk, $syntax, $partial = FALSE)
{
    // must be a hash
    if (! is_hash($chunk)) { return('not an object'); } // because it should be a JSON object in the file

    // make sure no unknown options are present
    foreach($chunk as $option => $value)
    {
        if (! array_key_exists($option, $syntax)) { return('unknown option "' . $option . '"'); }
    }

    // run through every option
    foreach($syntax as $option => $option_syntax)
    {
        // missing ?
        if (! array_key_exists($option, $chunk))
        {
            // partial configs only care about defined options
            if ($partial) { continue; }

            // mandatory flag and default value
            if ($option_syntax['mandatory']) { return('option "' . $option . '" must be defined');  }
            else { $chunk[$option] = $option_syntax['default']; }
        }

        // run 'check' function
        // A check function takes the value as its only argument and must return TRUE|FALSE, prime candidates for these are all the is_*() functions
        if (! $option_syntax['check']($chunk[$option])) { return('option "' . $option . '" has an invalid value'); }

        // run 'normalize' function
        // A normalize function takes the value as its only argument and must return the normalized value
        if (array_key_exists('normalize', $option_syntax))
        {
            $chunk[$option] = $option_syntax['normalize']($chunk[$option]);
        }
    }

    // all good
    return($chunk);
}




// check a templates hash
function config_templates_check($templates, $partial = FALSE)
{
    // must be a hash
    if (! is_hash($templates)) { return('not an object'); } // because it should be a JSON object in the file

    // check every template
    foreach($templates as $template_name => &$template)
    {
        $template = config_chunk_check($template, $GLOBALS['syntax_template'], $partial);
        if (is_string($template)) { return('template "' . $template_name . '": ' . $template); }

        // check every rule
        foreach([ '+', '-' ] as $polarity)
        {
            if (isset($template['rules' . $polarity]))
            {
                foreach($template['rules' . $polarity] as $i => &$rule)
                {
                    $rule = config_chunk_check($rule, $GLOBALS['syntax_rule'], $partial);
                    if (is_string($rule)) { return('template "' . $template_name . '", "rules' . $polarity . '" #' . $i+1 . ": " . $rule); }
                }
            }
        }
    }

    // all good
    return($templates);
}

?>