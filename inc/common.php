<?php
require_once('inc/config.php');

// basic init stuff
function init()
{
    // for debug
    ini_set('display_errors', 1);

    // load main config
    $config = config_main();

    // start PHP session, only used to cache infos about RRDs
    session_start([ 'cache_limiter' => 'private', 'cache_expire' => intval($config['refresh']/60 * 0.8) ]); // browsers can cache pages/images for 80% of the refresh time

    // enforce PHP session time limit of 80% of refresh time in case new RRDs appear
    if (isset($_SESSION['creation_time']) && ((time() - $_SESSION['creation_time']) > ($config['refresh'] * 0.8)))
    {
        session_unset();
        session_destroy();
    }
    $_SESSION['creation_time'] = $_SERVER['REQUEST_TIME'];

    return($config);
}



function fatal_error($http_code, $msg)
{
    http_response_code($http_code);
    echo $msg . "\n";
    exit;
}



// run a command, capture the output
function run($cmd)
{
    $fp = popen($cmd . ' 2>&1', 'r');
    if($fp === FALSE) { fatal_error(500, 'Could not create child'); }

    $output = stream_get_contents($fp);
    if($output === FALSE) { fatal_error(500, 'Could not fetch child output'); }

    $return_value = pclose($fp);
    if($return_value == -1) { fatal_error(500, 'Command failed'); }

    return([ $return_value, $output ]);
}



// list collectd hosts, then filter with $config['hosts']
function hosts()
{
    global $config;

    // find hosts in collectd
    list($hosts, $sources) = collectd_scan($config['datadir']);

    // filter hosts list if needed
    if (! is_null($config['hosts']))
    {
        $hosts = array_merge(array_intersect($hosts, $config['hosts'])); // array_merge() to re-index the array so $hosts[0] is guarantied to contain the firts host
    }
    if (count($hosts) == 0) { fatal_error(500, 'No hosts'); }

    return($hosts);
}






/********
 * HTTP *
 ********/

// fetch some request parameter, with sanity checks
function request_param($name, $is_mandatory, $default, $valid_values)
{
    // mandatory/default
    if (! array_key_exists($name, $_REQUEST))
    {
        // mandatory ?
        if ($is_mandatory) { fatal_error(400, 'Missing parameter "' . $name . '"'); }

        // use default
        else { $value = $default; }
    }
    else { $value = $_REQUEST[$name]; }

    // valid ?
    if ($valid_values !== NULL and ! in_array($value, $valid_values)) { fatal_error(400, 'Invalid parameter "' . $name . '": unknown value'); }

    // special tests
    if ($name == 'width' or $name == 'height')
    {
        if (! preg_match('/^[0-9]+$/', $value)) { fatal_error(400, 'Invalid parameter "' . $name . '": not an integer'); }
        $value = (int) $value;
        if ($value < 100 or $value > 4096) { fatal_error(400, 'Invalid parameter "' . $name . '": illegal value'); }
    }

    return($value);
}






/**********
 * is_*() *
 **********/

// number = int or float
function is_number($number) { return(is_int($number) or is_float($number)); }
// hosts = strings array (non-empty), or null
function is_hosts($hosts) { return((is_string_array($hosts) and count($hosts) != 0)  or is_null($hosts)); }
// hexcolor = hexadecimal triplet, without the #
function is_hexcolor($hexcolor) { return(is_string($hexcolor) and preg_match('/^[0-9a-fA-F]{6}$/', $hexcolor)); }
function is_hexcolor_array($array) { return(is_x_array($array, 'is_hexcolor')); }
// hash = array where all the keys are strings
function is_hash($hash) { return(is_array($hash) and is_string_array(array_keys($hash))); }
// hrule = hash with 3 keys: height, color and style. The values are: number, hexcolor and "solid"|"dashed"
function is_hrule($hrule) { return(is_hash($hrule) and count($hrule) == 3 and isset($hrule['height'], $hrule['color'], $hrule['style']) and
                                   is_number($hrule['height']) and is_hexcolor($hrule['color']) and in_array($hrule['style'], [ 'solid', 'dashed' ])); }
function is_hrule_array($array) { return(is_x_array($array, 'is_hrule')); }
// limit = number or null
function is_limit($limit) { return(is_number($limit) or is_null($limit)); }
// style = "lines", "stack" or "stack2"
function is_style($style) { return(in_array($style, [ 'lines', 'stack', 'stack2' ])); }
// multigraph = one of the 5 collectd identifiers, or null
function is_multigraph($multigraph) { return(in_array($multigraph, [ 'plugin', 'plugin_instance', 'type', 'type_instance', 'data_source' ]) or is_null($multigraph)); }
// source = string with 4 slashes, first/third/fifth parts cannot be empty
//////////function is_source($source) { return(is_string($source) and preg_match('#^.+/.*/.+/.*/.+$#', $source)); }
function is_source($source) { return(is_string($source) and preg_match('#^.+/.+:.+$#', $source)); }
// array/hash versions of is_string()
function is_string_array($array) { return(is_x_array($array, 'is_string')); }
function is_string_hash($hash) { return(is_hash($hash) and is_string_array($hash)); }
// timespan = integer + "m", "h" or "d"
function is_timespan($timespan) { return(preg_match('/^[0-9]+[mhd]$/', $timespan)); }
function is_timespan_hash($hash) { return(is_hash($hash) and is_x_array($hash, 'is_timespan')); }

// generic function to test if all the members of an array pass is_x()
function is_x_array($array, $is_x)
{
    if(! is_array($array)) { return(FALSE); }
    foreach($array as $value) { if(! $is_x($value)) { return(FALSE); } }
    return(TRUE);
}




/*****************
 * normalize_*() *
 *****************/

// replace timespans with theirs equivalent in seconds
function normalize_timespan_hash($hash)
{
    foreach ($hash as $name => &$value)
    {
        $matches = [];
        preg_match('/^([0-9]+)([mhd])$/', $value, $matches);
        $number = (int)$matches[1];
        $unit = $matches[2];
        if ($unit == 'm') { $value = $number * 60; }
        if ($unit == 'h') { $value = $number * 3600; }
        if ($unit == 'd') { $value = $number * 3600*24; }
        //        $value .= 's';
    }

    asort($hash); // sort from shortest to longest
    return($hash);
}


/**********
 * colors *
 **********/

// adjust a color's lightness, we use the HSL format to have better looking results than just scaling the RGB values
function hexcolor_lightness($hexcolor, $factor)
{
    // convert to RGB
    preg_match('/^(..)(..)(..)$/', $hexcolor, $matches);
    $RGB = [ hexdec($matches[1]), hexdec($matches[2]), hexdec($matches[3]) ];

    // convert to HSL, modify the L, then convert back to RGB
    $HSL = rgb2hsl($RGB);
    $HSL[2] *= $factor;
    $RGB = hsl2rgb($HSL);

    // convert back to hexcolor
    $hexcolor = sprintf("%02x%02x%02x", $RGB[0], $RGB[1], $RGB[2]);
    return($hexcolor);
}



// convert RGB array to HSL array
function rgb2hsl($RGB)
{
    list($R, $G, $B) = $RGB;

    // convert RGB from 0-255 to 0-1 scale (also converts int to float)
    $R /= 255;
    $G /= 255;
    $B /= 255;

    // see https://en.wikipedia.org/wiki/HSL_and_HSV for formula
    $M = max($R, $G, $B);
    $m = min($R, $G, $B);
    $C = $M - $m; // Chroma

    # Hue (0-360 angle)
    if     ($C == 0)  { $H = 0; } // acromatic (grey), should be undefined but any value is fine since we are at the center of the chromatic plane/hexagon: any angle is valid (but meaningless)
    elseif ($M == $R) { $H = fmod((($G - $B) / $C), 6); } // mostly red
    elseif ($M == $G) { $H = ($B - $R) / $C + 2; }        // mostly green
    elseif ($M == $B) { $H = ($R - $G) / $C + 4; }        // mostly blue
    $H *= 60;

    # Lightness (0-1)
    $L = ($M + $m)/2;

    # Saturation (0-1)
    if ($L == 0 or $L == 1) { $S = 0; }
    else { $S = $C / (1 - abs((2 * $L) - 1)); }

    return([ $H, $S, $L ]);
}



// convert HSL array to RGB array
function hsl2rgb($HSL)
{
    list($H, $S, $L) = $HSL;

    // see https://en.wikipedia.org/wiki/HSL_and_HSV for formula
    $C = (1 - abs((2 * $L) - 1)) * $S; // Chroma
    $H2 = $H / 60; // H', the "sector" (0-6) of the chromatic plane/hexagon
    $X = $C * (1 - abs(fmod($H2, 2) - 1));
    $m = $L - ($C / 2);

    // R, G, B
    if    ($H2 < 1) { list($R, $G, $B) = [ $C, $X, 0  ]; }
    elseif($H2 < 2) { list($R, $G, $B) = [ $X, $C, 0  ]; }
    elseif($H2 < 3) { list($R, $G, $B) = [ 0 , $C, $X ]; }
    elseif($H2 < 4) { list($R, $G, $B) = [ 0 , $X, $C ]; }
    elseif($H2 < 5) { list($R, $G, $B) = [ $X, 0 , $C ]; }
    else            { list($R, $G, $B) = [ $C, 0 , $X ]; }

    $R += $m;
    $G += $m;
    $B += $m;

    // convert RBG from 0-1 (float) to 0-255 (integer)
    $R = round($R * 255);
    $G = round($G * 255);
    $B = round($B * 255);

    return([ $R, $G, $B ]);
}




?>