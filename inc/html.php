<?php


// alias htmlspecialchars => hsc
function hsc($string) { return(htmlspecialchars($string)); }



// url generators
function html_url_overview($host) { return('index.php?host=' . hsc($host)); }
function html_url_detail($host, $template_name, $graph_name, $timespan_name)
{
    $url  = 'detail.php?host=' . hsc($host) . '&amp;template=' . hsc($template_name);
    $url .= '&amp;graph=' . hsc($graph_name) . '&amp;timespan=' . hsc($timespan_name);
    return($url);
}
function html_url_graph($host, $template_name, $graph_name, $timespan_name)
{
    $url  = 'graph.php?host=' . hsc($host) . '&amp;template=' . hsc($template_name);
    $url .= '&amp;graph=' . hsc($graph_name) . '&amp;timespan=' . hsc($timespan_name); // width/height will be added by JS
    return($url);
}








function html_page($title, $refresh, $body)
{
    // header
    $html  = '<!DOCTYPE html>';
    $html .= '<html>';
    $html .= '<head>';
    $html .= '<link rel="icon" type="image/png" href="favicon.png" />';
    $html .= '<title>' . hsc($title) . '</title>';
    $html .= '<link rel="stylesheet" href="css/style.css" type="text/css" />';
    $html .= '<meta http-equiv="refresh" content="' . $refresh . '" >';
    // JS to add width/height parameters at the end of graph urls, so we can adjust them to the browser's window size
    $html .= '<script>
window.onload = function()
{
  width_overview = Math.round(window.innerWidth/4);
  height_overview = Math.round(width_overview/2.2);
  document.querySelectorAll(".img_overview").forEach(function(v) { v.src = v.dataset.src + "&width=" + width_overview + "&height=" + height_overview; });
  width_detail = Math.round(window.innerWidth/1.5);
  height_detail = Math.round(width_detail/2.2);
  document.querySelectorAll(".img_detail").forEach(function(v) { v.src = v.dataset.src + "&width=" + width_detail + "&height=" + height_detail; });
};
</script>';
    $html .= '</head>';

    // body
    $html .= '<body>';
    $html .= $body;

    // footer
    $html .= '<p id="footer"><a href="https://gitlab.com/madchrist/collectd-html">collectd-html</a> - v2.0.0</p>';
    $html .= '</body>';
    $html .= '</html>';
    return($html);
}

?>