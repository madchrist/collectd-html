<?php
require_once('inc/collectd.php');

// convert a template into one or more graph definitions
//
// there are two main goals:
//  - detect how many graphs needed to be drawn (could be zero if no source can be found, could be more than one with 'multigraph')
//  - prepare things in a format easy to feed to rrdtool
// These information are needed on the overview page *before* we need the actual graphs
//
// returns a hash of graph definitions
//  - when multigraph is set, each key is a different value of the field pointed to by multigraph
//  - when multigraph is not set, there is only one key: "single"
// returns an empty array if no data is found at all
//
// a graph definition is the same as a template except:
// - 'title' is updated to sustitute %m
// - 'multigraph', 'rules+', 'rules-', 'position', 'disabled' and 'mirror-colors' are removed
// - 'datasets+' hash is added: [ "<substituted legend>" => [ "sources => [ "<RRD>:<DATA_SOURCE>", ... ], "color" => "<COLOR>" ], ... ]
// - 'datasets-' hash is added: same as above, but for negative data
function template2graphs($host, $template)
{
    global $config;
    $graphs = [];

    // list sources from collectd (the first returned value is the hosts list, useless here)
    list(, $sources) = collectd_scan($config['datadir']);

    // convert all sources into graph datasets
    foreach([ '+', '-' ] as $polarity)
    {
        foreach($template['rules' . $polarity] as $rule)
        {
            foreach($sources as &$source)
            {
                // skip wrong hosts
                if ($source['host'] != $host) { continue; }

                // skip sources not matching the rule
                $short_rrd = basename(dirname($source['rrd'])) . '/' . basename($source['rrd']); //  /var/lib/collectd/foo/load/load.rrd => load/load.rrd
                if (! fnmatch($rule['source'], $short_rrd . ':' . $source['data_source'])) { continue; }

                // sources can only be matched once
                if (isset($source['matched' . $polarity])) { continue; }
                $source['matched' . $polarity] = TRUE;

                // generate its graph-legend and graph-source (color is decided later)
                $glegend = str_replace([ '%P', '%p', '%T', '%t', '%D' ], [ $source['plugin'], $source['plugin_instance'], $source['type'], $source['type_instance'], $source['data_source'] ], $rule['legend']);
                $gsource = $source['rrd'] . ':' . $source['data_source'];

                // we have a matching source, find the graph it belongs to, creating it if neccessary
                if ($template['multigraph']) { $gname = $source[$template['multigraph']]; }
                else { $gname = 'single'; }

                // if this is the graph's first data, create the graph itself
                if (! array_key_exists($gname, $graphs))
                {
                    // new graph from template
                    $new_graph = $template;
                    if ($template['multigraph']) { $new_graph['title'] = str_replace('%m', $gname, $new_graph['title']); }
                    unset($new_graph['multigraph']);
                    unset($new_graph['rules+']);
                    unset($new_graph['rules-']);
                    unset($new_graph['position']);
                    unset($new_graph['disabled']);
                    unset($new_graph['mirrot-colors']);
                    $new_graph['datasets+'] = [];
                    $new_graph['datasets-'] = [];
                    $graphs[$gname] = $new_graph;
                }

                // add to correct datasets
                $datasets = 'datasets' . $polarity;
                $graphs[$gname][$datasets] = array_merge_recursive($graphs[$gname][$datasets], [ $glegend => [ 'sources' => [ $gsource ] ] ]);
            }
        }
    }

    foreach($graphs as $gname => &$graph)
    {
        // assign a color to each dataset
        $i = 0;
        foreach($graph['datasets+'] as &$dataset) { $dataset['color'] = $config['colors'][$i]; $i++; }
        if ($graph['mirror-colors']) { $i = 0; }
        foreach($graph['datasets-'] as &$dataset) { $dataset['color'] = $config['colors'][$i]; $i++; }

        // if there are both positives and negatives, add a grey line at zero to make the graph more legible
        if (count($graph['datasets+']) and count($graph['datasets-'])) { $graph['hrules'][] = [ 'height' => 0, 'color' => '666666', 'style' => 'solid' ]; }
    }

    return($graphs);
}


?>