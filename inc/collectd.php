<?php
require_once('inc/common.php');


// scan the datadir to find all the hosts and sources (combinaisons of host/plugin/plugin_instance/type/typ_instance/data_source)
function collectd_scan($datadir)
{
    // use cache if available
    if (isset($_SESSION['collectd_scan'])) { return($_SESSION['collectd_scan']); }

    // find all the RRDs
    $rrds = glob($datadir . "/*/*/*.rrd");
    if ($rrds === FALSE) { fatal_error(500, 'Error while scanning datadir'); }
    if (count($rrds) == 0) { fatal_error(500, 'No RRDs found'); }
    natsort($rrds); // natsort to have cpufreq-10 after cpufreq-9

    // find all hosts and sources
    $hosts = [];
    $sources = [];
    foreach($rrds as $rrd)
    {
        // extract host/plugin/plugin_instance/type/type_instance from the path
        $r = preg_match('#/(?<host>[^/]+)/(?<plugin>[^/-]+)-?(?<plugin_instance>|[^/]+)/(?<type>[^/-]+)-?(?<type_instance>|[^/]+)\.rrd$#', $rrd, $matches);
        if ($r !== 1) { fatal_error(500, 'Your RRDs do not follow Collectd\'s naming logic'); }
        $hosts[] = $matches['host'];
        $source = [ 'rrd'             => $rrd,
                    'host'            => $matches['host'],
                    'plugin'          => $matches['plugin'],
                    'plugin_instance' => $matches['plugin_instance'],
                    'type'            => $matches['type'],
                    'type_instance'   => $matches['type_instance'] ];

        
        // extract data_source with rrdinfo
        list($return_code, $rrdinfo) = run('rrdinfo --noflush ' . escapeshellarg($rrd));
        if ($return_code !== 0) { fatal_error(500, 'rrdinfo failed for: ' . $rrd); }
        foreach(explode("\n", $rrdinfo) as $line)
        {
            $r = preg_match('/^ds\[(?<data_source>.+)\]\.index/', $line, $matches);
            if ($r === 0) { continue; }

            // add to $sources
            $source['data_source'] = $matches['data_source'];
            $sources[] = $source;
        }
    }

    // cleanup hosts
    $hosts = array_unique($hosts); 

    // final result
    $scan = [ $hosts, $sources ];
    // save in cache
    $_SESSION['collectd_scan'] = $scan;
    return($scan);
}

?>