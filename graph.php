<?php
require_once('inc/config.php');
require_once('inc/common.php');
require_once('inc/template.php');

// basic init
$config = init();

// list hosts
$hosts = hosts();

// find host
$host = request_param('host', TRUE, NULL, $hosts);

// templates, with host override applied (if present)
$templates = config_templates($host);

// template
$template_name = request_param('template', TRUE, NULL, array_keys($templates));

// graph
$graphs = template2graphs($host, $templates[$template_name]);
$graph_name = request_param('graph', TRUE, NULL, array_keys($graphs));
$graph = $graphs[$graph_name];

// timespan
$timespans = $config['timespans-detail'];
$timespan_name = request_param('timespan', TRUE, NULL, array_keys($timespans));
$timespan = $timespans[$timespan_name];

// width and height
$width = request_param('width', TRUE, NULL, NULL);
$height = request_param('height', TRUE, NULL, NULL);

// convert graph into a rrdgraph command line
$rrdtool = graph2rrdtool($graph, $timespan, $width, $height);

// generate an svg image from the RRD
$svg = rrdtool_run($rrdtool);

// compress the svg into an svgz
$svgz = gzencode($svg);

// HTTP response
header('content-type: image/svg+xml');
header('content-encoding: gzip');
header('content-length: ' . strlen($svgz));
echo $svgz;
exit;








// convert a graph definition into a rrdgraph command line
function graph2rrdtool($graph, $timespan, $width, $height)
{
    global $config;

    $rrdtool = [ 'rrdtool', 'graph', '-', '--imgformat', 'SVG' ];
    $rrdtool = array_merge($rrdtool, $config['rrdtool-options']);

    // various graph options
    array_push($rrdtool, '--start', 'end-' . $timespan);  // time when the graph starts
    array_push($rrdtool, '--end',   'now');               // time when the graph ends
    array_push($rrdtool, '--width',  $width);             // width of the image, needed even for svg to have a sane size for the text
    array_push($rrdtool, '--height', $height);            // height of the image
    array_push($rrdtool, '--vertical-label', $graph['vertical-label']);         // the vertical text on the left of the graph
    if (! $graph['metric-prefix'])        { array_push($rrdtool, '-X', '0'); }  // metric prefix for Y axis values ?
    if (! is_null($graph['lower-limit'])) { array_push($rrdtool, '--lower-limit', $graph['lower-limit']); }  // lower limit for the graph
    if (! is_null($graph['upper-limit'])) { array_push($rrdtool, '--upper-limit', $graph['upper-limit']); }  // upper limit

    // hrules
    foreach($graph['hrules'] as $hrule)
    {
        $line = 'HRULE:' . $hrule['height'] . '#' . $hrule['color'];
        if($hrule['style'] == 'dashed') { $line .= '::dashes'; }
        $rrdtool[] = $line;
    }


    // graph datasets+, then datasets-
    // don't generate legends because their order is different
    foreach([ '+', '-' ] as $polarity)
    {
        // each dataset will be one line/area in the graph, but can be made from several RRDs so we need to additionate their values first
        // we also need 4 variants of the data: min, average, max, last
        // all four are used in the legend to display stats, but "average" is also used for the actual graph
        $i = 0;
        foreach($graph['datasets' . $polarity] as $legend => $dataset)
        {
            // a unique id for the CDEFs
            $id = md5($polarity . $legend);

            // the four variants only differ by their CF (Consolidation Function)
            foreach([ 'min', 'average', 'max', 'last' ] as $variant)
            {
                // we may need to merge multiple sources, so start with something empty
                $line = 'CDEF:' . $id . '_' . $variant . '=UNKN';

                // then add each source to it
                foreach($dataset['sources'] as $j => $source)
                {
                    // fetch the values from the RRD and consolidate them
                    $rrdtool[] = 'DEF:' . $id . '_' . $j . '_' . $variant . '=' . $source . ':' . strtoupper($variant);

                    // additionate these values to the main CDEF
                    $line .= ',' . $id . '_' . $j . '_' . $variant . ',ADDNAN';
                }

                // apply the user requested factor
                $line .= ',' . $graph['factor'] . ',*';
                $rrdtool[] = $line;
            }

            // use average, possibly negated, to graph
            if ($polarity == '+') { $rrdtool[] = 'CDEF:' . $id . '_graph=' . $id . '_average';      }
            else                          { $rrdtool[] = 'CDEF:' . $id . '_graph=' . $id . '_average,-1,*'; } // only the graph is negated, the stats stay the same

            // we need a CDEF for later, doesn't matter which one, but it needs to exist
            $dummy_source = $id . '_graph';
            

            // draw the data

            // style "lines" => line
            if ($graph['style'] == 'lines')
            {
                $rrdtool[] = 'LINE:' . $id . '_graph#' . $dataset['color'];
            }

            // style "stack" => area (stacked)
            elseif ($graph['style'] == 'stack')
            {
                $line = 'AREA:' . $id . '_graph#' . $dataset['color'];
                if ($i != 0) { $line .= ':STACK'; } // really usefull for the first negative dataset because we don't want to stack on the last positive dataset, we want to stack on 0
                $rrdtool[] = $line;
            }

            // style "stack2" => area a bit lighter, and line on top a bit darker (stacked)
            elseif ($graph['style'] == 'stack2')
            {
                // area
                $line = 'AREA:' . $id . '_graph#' . hexcolor_lightness($dataset['color'], 1.15);
                if ($i != 0) { $line .= ':STACK'; }
                $rrdtool[] = $line;

                // the outline is just a line of zeros stacked on top of the actual data
                //   DUP,UNKN,IF transforms zeros into UNKNs to remove the outline when the data is zero (looks better)
                //   0,* transform any non-UNKN value into zeros because the outline is just a zero-height line on top of the data area
                //                $rrdtool[] = 'CDEF:' . $id . '_zero=' . $id . '_graph,DUP,UNKN,IF,0,*';
                $rrdtool[] = 'CDEF:' . $id . '_zero=' . $id . '_graph,0,*';
                $rrdtool[] = 'LINE:' . $id . '_zero#' . hexcolor_lightness($dataset['color'], 0.85) . '::STACK';
            }

            $i++;
        }
    }

    // legends
    //
    // first:  column headers (literally "Min Avg Max Last")
    // second: positive datasets in reverse order
    // third:  negative datasets in regular order
    //
    // HEADERS
    // P3
    // P2
    // P1
    // N1
    // N2
    // N3

    // we need a dummy CDEF full of UNKN to add arbitrary legends, but we need to start from actual data, this is a limitation of rrdtool
    $rrdtool[] = 'CDEF:dummy=' . $dummy_source . ',UNKN,*'; // we multiply every value by UNKN resulting in "dummy" being all UNKNs

    // we also need the length of the longest legend, this is needed to align the text at the bottom of the graph
    $legend_length = 0;
    foreach($graph['datasets+'] as $legend => $sources) { $legend_length = max($legend_length, strlen($legend)); }
    foreach($graph['datasets-'] as $legend => $sources) { $legend_length = max($legend_length, strlen($legend)); }
    $legend_length; 
    
    // legend header
    $rrdtool[] = 'COMMENT:' . str_repeat(' ', $legend_length + 2); // for the optional arrow at the end
    $rrdtool[] = 'COMMENT:Min';
    $rrdtool[] = 'COMMENT:Avg';
    $rrdtool[] = 'COMMENT:Max';
    $rrdtool[] = 'COMMENT:Last \j';

    // datasets+, last to first
    // datasets-, first to last    
    foreach([ '+', '-' ] as $polarity)
    {
        $datasets = $graph['datasets' . $polarity];
        if ($polarity == '+') { $datasets = array_reverse($datasets); }
        
        foreach($datasets as $legend => $dataset)
        {
            $id = md5($polarity . $legend);
            // draw an invisible line, just for the legend
            // made invisible by being all UNKN values, which are not drawn
            $line = 'LINE:dummy#' . $dataset['color'] . ':' . sprintf('% -' . $legend_length . 's', $legend);

            // add little arrows at the end to make the legend more intuitive when there are positive AND negative datasets
            if     ($polarity == '+' and ! empty($graph['datasets-'])) { $line .= ' ▲'; }
            elseif ($polarity == '-' and ! empty($graph['datasets+'])) { $line .= ' ▼'; }
            else { $line .= '  '; }
            $rrdtool[] = $line;
            
            // stats (min, avg, max, last)
            if ($graph['metric-prefix']) { $prefix = '%s'; } else { $prefix = ' '; }
            $rrdtool[] = 'VDEF:'   . $id . '_min_min='         . $id . '_min,MINIMUM';
            $rrdtool[] = 'GPRINT:' . $id . '_min_min:%8.2lf'         . $prefix;
            $rrdtool[] = 'VDEF:'   . $id . '_average_average=' . $id . '_average,AVERAGE';
            $rrdtool[] = 'GPRINT:' . $id . '_average_average:%8.2lf' . $prefix;
            $rrdtool[] = 'VDEF:'   . $id . '_max_max='         . $id . '_max,MAXIMUM';
            $rrdtool[] = 'GPRINT:' . $id . '_max_max:%8.2lf'         . $prefix;
            $rrdtool[] = 'VDEF:'   . $id . '_last_last='       . $id . '_last,LAST';
            $rrdtool[] = 'GPRINT:' . $id . '_last_last:%8.2lf'       . $prefix . '\j';
        }
    }

    return($rrdtool);
}



function rrdtool_run($rrdtool)
{
    // call rrdtool
    $finalcmd = '';
    foreach ($rrdtool as $arg) { $finalcmd .= escapeshellarg($arg) . ' '; }

    //if (array_key_exists('debug', $_REQUEST)) { print($finalcmd); exit; }
    list($return_code, $output) = run($finalcmd);
    if ($return_code !== 0) { fatal_error(500, 'Failed to generate graph'); }

    return($output);
}



?>