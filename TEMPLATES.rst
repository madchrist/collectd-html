.. role:: json(code)
   :language: json

=========
Templates
=========

Templates define which RRDs to use, how to process the data inside them and how to graph the data.

The base templates are stored in ``config/templates.json``.
This file can be used as an example for the syntax.

Each host is also allowed an override in ``config/templates-<hostname>.json`` for host-specific customizations.
See `Host override`_ for more informations.

These files should contain a single JSON object where each key is a template name and each value is a template object.

Comments in the form of lines starting with a double-slash ``//`` are allowed.


--------
Template
--------

A template object contains the following keys:


disabled
--------

Ignore the template.

*Default:* :json:`false`



position
--------

The position of the template's graphs in the overview page.

The template at the top is the one with the lowest ``position``.
If multiple templates have the same ``position``, they will be sorted by their `title`_.

Example: :json:`10`

*Default:* :json:`999999`



title
-----

The graph's title.

When `multigraph`_ is active, ``%m`` will be replaced in each graph by the instance of the multigraph field.

Examples:

* :json:`"Memory"`
* :json:`"Traffic %m"` as used in the *interface* template, where ``%m`` is replaced with the name of the interface.

*Default:* :json:`"missing title"`



vertical-label
--------------

The text written vertically on the left of the graph, usually a unit.

Example: :json:`"bytes/s"`

*Default:* :json:`"missing vertical-label"`



hrules
------

Fixed horizontal lines. Typically used to show a threshold value.

Takes the form of an array of objects, each object defines a line and contains 3 keys:

* ``height``: the height of the line, this is in the same unit as the data, not pixels.
* ``color``: the color of the line (hexadecimal).
* ``style``: the style of the line: :json:`"solid"` or :json:`"dashed"`.

Example:

A dashed orange line at 42 and a solid red line at 50.

.. code-block:: json

    [ { "height": 42, "color": "FFA500", "style": "dashed" },
      { "height": 50, "color": "FF0000", "style": "solid" } ]


*Default:* :json:`[]`




lower-limit / upper-limit
-------------------------

Lock the bottom/top of the graph to a specific value.

If set to :json:`null` (the default) the limit is automatically calculated to allow all the data to be visible.
The value is in the same unit as the data, not pixels.

Example:

| The `df` template uses a ``lower-limit`` of :json:`0` and an ``upper-limit`` of :json:`100`.
| This locks the graph in the 0-100% range, providing a more intuitive depiction of drive capacity at a glance.

*Default:* :json:`null`



metric-prefix
-------------

Use metric prefixes (k,M,G,T,...) for all numeric values.

*Default:* :json:`false`



style
-----

The style used to draw the data.

Possible values:

* ``"lines"`` each dataset is drawn as a line.
* ``"stack"`` each dataset is drawn as a filled-in area, stacked on top of the previous one.
* ``"stack2"`` same as ``stack`` but with an extra dark line on top of the area.

*Default:* :json:`"lines"`



mirror-colors
-------------

Use the same colors for positive and negative data.

Example: if there are 2 positive datasets and 2 negative datasets.

* :json:`true` the positive datasets use colors #1 and #2, the negative datasets also use colors #1 and #2, this gives the graph a *mirrored* look.
* :json:`false` the positive datasets use colors #1 and #2, the negative datasets use colors #3 and #4.

*Default:* :json:`false`



factor
------

A number to multiply data values with.

Examples:

* :json:`8` multiply everything by 8 because data is in bytes/s but you want bits/s.
* :json:`0.125` opposite of above: divide by 8.

*Default:* :json:`1`



multigraph
----------

A way to generate multiple graphs from a single template.

Possible values:

* :json:`"plugin"`
* :json:`"plugin_instance"`
* :json:`"type"`
* :json:`"type_instance"`
* :json:`"data_source"`
* :json:`null` no multigraph

One graph will be generated for each different value of the requested field.

Collectd names the RRD with the logic: ``<plugin>[-<plugin_instance>]/<type>[-<type_instance>].rrd``, the *data_sources* are in the RRDs themselves.
*plugin_instance* and *type_instance* are not always present, for example the load plugin (``load/load.rrd``) has neither *plugin_instance* nor *type_instance*.

Example: :json:`"plugin_instance"` as used by the `interface` template to create one graph per interface (because collectd's *interface* plugin puts each interface in a different `plugin_instance`).

Note that only what is matched in *rules* is used: a value present on the system but not matched by any *rules*, will not generate a new graph.

*Default:* :json:`null`



rules+/rules-
-------------

An array of rules to find the data and to define their legends.

The order is preserved in the graph.

| ``rules+`` is for data graphed positively (i.e.: normal).
| ``rules-`` is for data graphed negatively, as if multiplied by -1 (the legend values are not negated).

*Default:* []


rule
^^^^
A rule defines which RRD(s) to use and what its legend will be in the graph.

It is an object with two keys:


source
""""""

A filter to find the data from collectd, in the form: :json:`"<relative_path_of_rrd>:<data_source>"`.
`Glob patterns <https://en.wikipedia.org/wiki/Glob_(programming)#Syntax>`_ can be used.

* ``<relative_path_of_rrd>`` is the part of the path after the hostname, e.g.: ``load/load.rrd`` or ``disk-sda/disk_octets.rrd``.
* ``<data_source>`` is defined inside the RRD, they can be listed with ``rrdinfo foo/bar.rrd | grep index``.

Data can only be matched once, if two rules match the same thing, only the first rule gets it.

Examples:

* :json:`"load/load.rrd:*"` matches the 3 *data_sources* in this RRD (``shortterm``, ``midterm`` and ``longterm``). If we only want ``shortterm`` we can use :json:`"load/load.rrd:shortterm"`.
* :json:`"memory/memory-slab_*.rrd:value"` matches ``memory/memory-slab_recl.rrd`` and ``memory/memory-slab_unrecl.rrd`` (but only their ``value`` *data_source*).
* :json:`"i*face-eth[3-4]/if_o*:*"` matches ``interface-eth3/if_octets.rrd`` and ``interface-eth4/if_octets.rrd`` (and all of their *data_sources*).

There is no default value, this field is mandatory.



legend
""""""

The name appearing in the graph's legend.


It accepts the following substitutions:

* *%P* plugin
* *%p* plugin_instance
* *%T* type
* *%t* type_instance
* *%D* data_source

When using substitutions, multiple datasets can be created if their legends end up being different. Each one will be graphed independently.

For example, if only two interfaces exist (``eth0`` and ``eth1``) and we have the following rule:

.. code:: json

     { "source": "interface-*/if_octets.rrd:rx", "legend": "%p download" }

Then two datasets will be graphed:

* ``interface-eth0/if_octet.rrd:rx`` with ``eth0 download`` as legend
* ``interface-eth1/if_octet.rrd:rx`` with ``eth1 download`` as legend

| Conversely, if two sources use the same legend, they will be merged by additioning the data.
| For example, if we have the following two rules:

.. code-block:: json

    { "source": "memory/memory-slab_recl.rrd:value",   "legend": "slab" },
    { "source": "memory/memory-slab_unrecl.rrd:value", "legend": "slab" }


| Then a single dataset will be graphed with the legend ``slab``. Its values will be the sum of ``memory/memory-slab_recl.rrd:value`` and ``memory/memory-slab_unrecl.rrd:value``.
| The rule :json:`{ "source": "memory/memory-slab_*.rrd:value", "legend": "slab" }` would have exactly the same result.

*Default:* :json:`"%P-%p/%T-%t/%D"`




-------------
Host override
-------------

Each host can use ``config/templates-<hostname>.json`` to modify the base templates in ``config/templates.json``. The modifications only affect that host.

| The hostname must be amongst the subdirectories of *<datadir>*.
| Example: if ``<datadir>/foo`` exists, ``config/templates-foo.json`` can be used.

The override uses the same syntax as ``config/templates.json`` but only what is modified needs to be present.

* Template objects only present in the override are simply added. This is used to add new templates for the host.
* Template objects present in both are merged: values from the base template are replaced by those in the override.
* ``rules+``/``rules-`` is no exception, you cannot *append* a rule, they whole array is replaced.

Example 1: disable the *df* template

.. code-block:: json

    { "df":
      { "disabled": true }
    }


Example 2: modify the *interface* template to use bits/s instead of bytes/s

.. code-block:: json

    { "interface":
      { "vertical-label": "bits/s",
        "factor": 8
      }
    }


Example 3: give proper names to fans and temperatures from the *sensors_fanspeed* and *sensors_temperature* templates

.. code-block:: json

    { "sensors_fanspeed":
      { "rules+": [ { "source": "sensors-nct6687-isa-0c40/fanspeed-fan1.rrd:value", "legend": "cpu" },
                    { "source": "sensors-amdgpu-pci-8a00/fanspeed-fan1.rrd:value",  "legend": "gpu" } ]
      },

      "sensors_temperature":
      { "rules+": [ { "source": "sensors-k10temp-pci-00d1/temperature-temp1.rrd:value", "legend": "cpu" },
                    { "source": "sensors-amdgpu-pci-8a00/temperature-temp1.rrd:value",  "legend": "gpu" } ]
      }
    }
