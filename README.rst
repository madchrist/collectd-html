=============
collectd-html
=============


Overview
========
A web page showing graphs of the data gathered by `collectd <https://collectd.org>`_.

The graphs are created from collectd's RRDs according to json templates.

Perfect for a single machine or a small network.


Screenshots
===========
* `Overview page <screenshots/overview.png>`_, shows graphs for a specific host. Note the tabs at the top to change host.
* `Detail page <screenshots/detail_eth0_day.png>`_ for interface eth0, showing a day.
* `Detail page <screenshots/detail_eth0_week.png>`_ for interface eth0, showing a week.


Requirements
============
* PHP 8+
* Collectd with the `RRDtool plugin <https://collectd.org/wiki/index.php/Plugin:RRDtool>`_
* `RRDtool <https://oss.oetiker.ch/rrdtool>`_


Installation
=============
1. Copy this directory, and all its content, somewhere in your website hierarchy.
2. Open it in your browser, you should see some graphs right away if your collectd's configuration is standard enough.


Configuration
=============
The configuration is stored in ``config/config.json``.

See ``CONFIG.rst`` for more informations.


Templates
=========
The main templates are stored in ``config/templates.json``.

See ``TEMPLATES.rst`` for more informations.


Host override
-------------
Each host can also have an override in ``config/templates-<hostname>.json`` (e.g.: ``config/templates-foo.json``) which, if present, is applied on top of the main templates to allow host-specific customizations.

See ``TEMPLATES.rst`` for more informations.
