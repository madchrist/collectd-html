.. role:: json(code)
   :language: json

======
Config
======

The configuration is stored in ``config/config.json``, it should consist of a single JSON object, with its keys described below.

Comments in the form of lines starting with a double-slash ``//`` are allowed.


datadir
-------

Directory where the RRDs are stored.

This is defined in your ``collectd.conf``, usually as ``BaseDir``.
Collectd's default is ``/var/lib/collectd``.

This directory must contain hostnames subdirectories.
For example, the RRD for the ``load`` plugin of host ``foo``, is expected to be in: ``<datadir>/foo/load/load.rrd``.

*Default:* :json:`"/var/lib/collectd"`




hosts
-----

Array of hosts to show, the order will be preserved.

Can be :json:`null`, in which case every host detected in ``datadir`` is shown, in alphabetical order.

Example: :json:`[ "foo", "bar" ]` only show hosts *foo* and *bar*, in that order

*Default:* :json:`null`



colors
------

Array of colors, to draw the lines/areas in the graphs.

The first color is used by the first line/area in the graph, and so on.

Colors are `hexadecimal triplets <https://en.wikipedia.org/wiki/Web_colors#Hex_triplet>`_.

*Default:*

.. code:: json

    [ "00CC00", "0066B3", "FF8000", "FFCC00", "330099", "990099", "CCFF00", "FF0000",
      "808080", "008F00", "00487D", "B35A00", "B38F00", "6B006B", "8FB300", "B30000",
      "BEBEBE", "80FF80", "80C9FF", "FFC080", "FFE680", "AA80FF", "EE00CC", "FF8080",
      "666600", "FFBFFF", "00FFCC", "CC6699", "999900" ]



rrdtool-options
---------------

Array of options for the rrdtool command, to be used for every graph generated.

Note: if using rrdcached, rrdtool has the option ``--daemon <address>`` to force a sync before graphing.

*Default:*

.. code:: json

    [ "--slope-mode",
      "--color", "BACK#F0F0F0",
      "--color", "FRAME#F0F0F0",
      "--color", "CANVAS#FFFFFF",
      "--color", "FONT#666666",
      "--color", "AXIS#CFD6F8",
      "--color", "ARROW#CFD6F8",
      "--border", "0" ]



timespans-overview
------------------

Object describing the spans of time of the graphs on the overview page, each timespan defined will add a column of graphs

The keys are user-friendly names, while the values are lengths of time in either minutes, hours or days (e.g., ``90m``, ``33h`` or ``8d``)

*Default:* :json:`{ "day": "24h", "week": "7d" }`



timespans-detail
----------------

Object describing the spans of time of the graphs on the detail page, each timespan defined will add a tab.

| Same format as ``timespans-overview``.
| Every timespan from ``timespans-overview`` is automatically added to this list.

*Default:* :json:`{ "hour": "1h", "month": "31d", "year": "365d" }`



refresh
-------

Number of seconds to wait before automatically reloading the page, to keep the graphs up to date.

*Default:* :json:`600`
